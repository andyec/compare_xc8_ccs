/* 
 * File:   uart.h
 * Author: andy
 *
 * Created on December 6, 2017, 5:39 PM
 */

#ifndef UART_H
#define	UART_H

#include <xc.h>
#include <stdint.h>

void uart_init(void);
uint8_t uart_readchar(void);
void uart_putc(uint8_t txData);

#endif	/* UART_H */


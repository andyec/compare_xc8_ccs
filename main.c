#if defined(__XC8)

#include <xc.h>
#include <stdint.h>
#include "uart.h"

#else

#include <16lf18345.h>
#include <stdint.h>

#pin_select U1TX=PIN_C3
#pin_select U1RX=PIN_B5

#use delay (clock=16000000)
#use rs232(uart1, baud=4800, errors)
#endif

#define DELTA 0x9e3779b9
#define MX (((z>>5^y<<2) + (y>>3^z<<4)) ^ ((sum^y) + (key[(p&3)^e] ^ z)))

static uint8_t test_key[16] = {0x9e, 0x37, 0x79, 0xb9, 0x9b, 0x97, 0x73, 0xe9, 0xb9, 0x79, 0x37, 0x9e, 0x6b, 0x69, 0x51, 0x56};
static uint8_t test_vector[26] = {25, 0x10, 0x69, 0x03, 0x42, 0xf4, 0x50, 0x54, 0xa7, 0x08, 0xc4, 0x75, 0xc9, 0x1d, 0xb7, 0x77, 0x61, 0xbc, 0x01, 0xb8, 0x15, 0xfd, 0x2e, 0x48, 0x94, 0xd1};

/* XXTEA encryption */
static void btea(uint32_t *v, int8_t n, uint32_t* key)
{
    uint32_t y, z, sum;
    unsigned p, rounds, e;
    if (n > 1)
    { /* Coding Part */
        rounds = 6 + 52 / n;
        sum = 0;
        z = v[n - 1];
        do
        {
            sum += DELTA;
            e = (sum >> 2) & 3;
            for (p = 0; p < n - 1; p++)
            {
                y = v[p + 1];
                z = v[p] += MX;
            }
            y = v[0];
            z = v[n - 1] += MX;
        } while (--rounds);
    }
    else if (n < -1)
    { /* Decoding Part */
        n = -n;
        rounds = 6 + 52 / n;
        sum = rounds*DELTA;
        y = v[0];
        do
        {
            e = (sum >> 2) & 3;
            for (p = n - 1; p > 0; p--)
            {
                z = v[p - 1];
                y = v[p] -= MX;
            }
            z = v[n - 1];
            y = v[0] -= MX;
            sum -= DELTA;
        } while (--rounds);
    }
}

void main(void)
{
    uint8_t i;

#if defined(__XC8)
    /* Set up the oscillator for 16 MHz*/
    OSCCON1 = 0x60;
    OSCCON3 = 0x00;
    OSCEN = 0x00;
    OSCFRQ = 0x06;
    OSCTUNE = 0x00;

    /* Set up the TRIS registers */
    LATA = 0;
    LATB = 0;
    LATC = 0;

    TRISA = 0b00001000;
    TRISB = 0b10110000;
    TRISC = 0b00010010;

    ANSELA = 0;
    ANSELB = 0;
    ANSELC = 0;

    RXPPSbits.RXPPS = 0x0D; //RB5->EUSART:RX;
    RC3PPSbits.RC3PPS = 0x14; //RC3->EUSART:TX;

    /* Set up UART */
    uart_init();
#else
    setup_oscillator(OSC_HFINTRC_16MHZ);
    setup_adc_ports(NO_ANALOGS);
#endif

    /* Encrypt data */
    btea(&test_vector[2], 6, test_key);

    /* Transmit the result via UART */
    for (i = 0; i < 26; i++)
    {
#if defined(__XC8)

        uart_putc(test_vector[i]);
#else
        putc(test_vector[i]);
#endif
    }

    while (1);

}

# README #

This code has been written to facilitate a comparison between the CCS C PIC compiler (www.ccsinfo.com) and Microchip's XC8 compiler (http://www.microchip.com/mplab/compilers).

\#defines have been used such that the code will compile using either CCS or XC8.

Tested with CCS 5.074 and XC8 1.44.

Clone this repo and open it up in MPLAB X.

See http://www.burningimage.net/cattrack/ccs-c-microchip-xc8-compiler-comparison/ for more info and background.